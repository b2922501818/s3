-- CRUD Operations
--Create
--Retrieve/Read
--Update
--Delete

--Insert Records (Create)

--to insert artists record into the artist table:
--INSERT INTO <table_name>(field_name) VALUES (Value);
INSERT INTO artists(name) VALUES ("Post Malone");
INSERT INTO artists(name) VALUES ("Aegis");
INSERT INTO artists(name) VALUES ("Salbakutah");
INSERT INTO artists(name) VALUES ("Journey");
INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("BTS");
INSERT INTO artists(name) VALUES ("Eraserheads");

--to insert album records into the albums table:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("E5C4PE", "1981-06-31", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Wings", "2016-10-10", 6);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("CUTTERPILLOW", "1995-12-08", 7);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("HOLLYWOOD'S BLEEDING","2019",1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Halik","1998",2);

--to insert song records in the songs table
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Open Arms', '00:03:18','rock',1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('You Belong With Me', '00:03:52','country',2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Blood Sweat & Tears', '00:03:37','moombahton',3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Ang Huling El Bimbo', '00:07:30','rock',4);

--Read Operation/Retrieving Records

--Display all the fields from songs table
SELECT * FROM songs;

--Display title and genre of all the songs
--SELECT <field_name> FROM <table_name>;
SELECT song_name, genre FROM songs;

--Display the title of all rock songs
SELECT song_name FROM songs WHERE genre = "rock";
SELECT song_name FROM songs WHERE genre = "kpop";

--Display the title and length of the moombahton songs that are more than 3:00 minutes

SELECT song_name, length FROM songs WHERE length > 300 AND genre = "moombahton";

--Updating Records
--Update the length of Blood Sweat and Tears to 00:04:00
--UPDATE <table_name> SET <field_name> = <value> WHERE <val_field> = <Value>;

UPDATE songs SET length = 400 WHERE song_name = "Blood Sweat & Tears";
UPDATE songs SET length = 500 WHERE song_name = "Blood Sweat & Tears";

--Delete Records
--Delete all moombahton songs that are more than 4:00 minutes
--DELETE FROM <table_name> WHERE <val_field>;
DELETE FROM songs WHERE genre = "moombahton" AND length > 400;

DELETE FROM songs;


--Add multiple values
--INSERT INTO <table_name> (<column1>,<column2>) VALUES (value1,value2), (value1, value2);